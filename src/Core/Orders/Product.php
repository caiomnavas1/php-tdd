<?php

namespace Core\Orders;

class Product
{
    public function __construct(
        protected string $id,
        protected string $name,
        protected float $price,
        protected int $total)
    {}

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float|float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int|int
     */
    public function getTotal()
    {
        return $this->total;
    }

    public function total(): float
    {
        return $this->price * $this->total;
    }

    public function totalWithTax10(): float
    {
        return ($this->total() *0.1) + $this->total();
    }
}