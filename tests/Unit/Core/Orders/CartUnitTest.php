<?php

namespace Tests\Core\Orders;

use Core\Orders\Cart;
use Core\Orders\Product;
use PHPUnit\Framework\TestCase;
use function Core\Orders\total;

class CartUnitTest extends TestCase
{
    public function testCart()
    {
        $cart = new Cart();
        $cart->add(product: new Product(
        id: '1',
            name: 'Boné',
            price: 12.0,
            total: 1,
        ));
        $cart->add(product: new Product(
        id: '2',
            name: 'Chapéu',
            price: 20,
            total: 1,
        ));

        $this->assertEquals(32, $cart->total());
        $this->assertCount(2, $cart->getItems());
    }

    public function testCartTotal()
    {
        $cart = new Cart();
        $cart->add(product: new Product(
        id: '1',
            name: 'Boné',
            price: 12.0,
            total: 1,
        ));
        $cart->add(product: new Product(
        id: '1',
            name: 'Boné',
            price: 12.0,
            total: 1,
        ));
        $cart->add(product: new Product(
        id: '2',
            name: 'Chapéu',
            price: 20,
            total: 1,
        ));

        $this->assertEquals(44, $cart->total());
        $this->assertCount(2, $cart->getItems());
    }

    public function testCartEmpty()
    {
        $cart = new Cart();

        $this->assertEquals(0, $cart->total());
        $this->assertCount(0, $cart->getItems());
    }
}