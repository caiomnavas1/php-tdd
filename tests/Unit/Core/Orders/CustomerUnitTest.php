<?php

namespace Tests\Core\Orders;

use Core\Orders\Customer;
use PHPUnit\Framework\TestCase;

class CustomerUnitTest extends TestCase
{
    public function testAttributes()
    {
        $customer = new Customer(
            name: "Caio Navas"
        );
        $this->assertEquals('Caio Navas', $customer->getName());

        $customer->changeName(
            name: "New Name"
        );

        $this->assertEquals('New Name', $customer->getName());

        $this->assertTrue(true);
    }
}